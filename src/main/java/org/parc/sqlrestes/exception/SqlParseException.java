package org.parc.sqlrestes.exception;

/**
 * Created by xusiao on 2018/5/3.
 */
public class SqlParseException  extends Exception {

    public SqlParseException(String message) {
        super(message);
    }


    private static final long serialVersionUID = 1L;

}
